// const CompressionPlugin = require("compression-webpack-plugin");
module.exports = {
    publicPath: '/',
    pages: {
      index: {
        entry: "src/main.js",
        template: "public/index.html",
        filename: "index.html"
      }
    },
    devServer: {
      clientLogLevel: "silent",
      hot: false,
      contentBase: "dist",
      compress: true,
      open: true,
      overlay: {warnings: false, errors: true},
      quiet: true,
      public: '0.0.0.0',
      port: 8080,
      publicPath: '/',
      sockPath: '/sockjs-node/',
      disableHostCheck: true,
      watchOptions: {
        poll: false,
        ignored: /node_modules/
      },
    },
    chainWebpack: config => {
      // config.plugins.delete('prefetch');
      // // and this line 
      // config.plugin('CompressionPlugin').use(CompressionPlugin);
      config.module
        .rule("vue")
        .use("vue-loader")
        .loader("vue-loader")
        .tap(options => {
          options.compilerOptions.preserveWhitespace = true;
          return options;
        });
    },
    productionSourceMap: false,
    lintOnSave: false,
    pluginOptions: {}
};
