//Celular
$(document).on('focusin keyup paste', '#strcelular', function () {
    $("#strcelular").unmask();
    this.value = this.value.replace(/[^0-9]/g, '');
});
$(document).on("focusout", "#strcelular", function () {
    $('#strcelular').mask('(99)99999999?9');
});
//CPF
$(document).on('focusin keyup paste', '#strcpf', function () {
    $("#strcpf").unmask();
    this.value = this.value.replace(/[^0-9]/g, '');
});
$(document).on("focusout", "#strcpf", function () {
    $("#strcpf").mask('999.999.999-99');
});
//RG
$(document).on('focusin keyup paste', '#strrg', function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});
