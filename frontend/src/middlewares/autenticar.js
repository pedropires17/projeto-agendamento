import { checaValidadeToken, Deslogar, DeslogarSilencioso, FixToast, AbrePagina } from './funcoes';
export default (to, from, next) => {
  //Checa se existe token armazenado
  if (localStorage.getItem("userToken") != null 
      && localStorage.getItem("userToken").length > 0) {
      //Se exite, checa se o token é válido
      let checagemToken = checaValidadeToken(localStorage.getItem("userToken"));
      //Se não for Derruba
      if(!checagemToken) Deslogar();
      //Se for válido
      else {
        FixToast();
        if(to.path === '/') AbrePagina('/agendar');
        else next();
      }
  } else {
    (from.path === '/' || from.path.includes('/adm'))? DeslogarSilencioso() :  Deslogar();
  }
};