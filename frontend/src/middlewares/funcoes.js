import moment from 'moment';
import jwt from "jsonwebtoken";
import router from "../router";
import { AvisoErroPromise, AvisoDatatables } from "./avisos";
/* Fix Toast */
const FixToast = ()=>{
  //Fix Toast - Remove o overlay quando a rota muda enquanto o toast é exibido
  let detecta = document.getElementsByClassName("swal2-container");
  if(detecta && detecta.length > 0) detecta[0].remove();
}
/* Checa validade do Token */
const checaValidadeToken = (token) => {
  if (token) {
    try {
      var publicKey = process.env.VUE_APP_ENV_CHAVE_PUBLICA;
      return jwt.verify(token, publicKey, { algorithms: ["RS256"] });
    } catch (e) {
      return false;
    }
  } else {
    return false;
  }
}
const Derrubar = () => {
  //Limpa do storage os dados
  localStorage.removeItem("userToken");
  router.push('/login');
}
/* Desloga do Sistema exibindo aviso */
const Deslogar = () => {
  //Mensagem de erro
  AvisoErroPromise('Sua sessão expirou faça o login novamente')
  .then(() => Derrubar());
  return;
}
/* Desloga do Sistema exibindo aviso */
const DeslogarSilencioso = () => Derrubar();
/* Valida o Token */
const ValidaToken = () => {
  if (localStorage.getItem("userToken") != null
      && localStorage.getItem("userToken").length > 0) {
    //Checa se o token é válido
    if (!checaValidadeToken(localStorage.getItem("userToken"))){
      Deslogar();
      throw new Error('Sua sessão expirou faça o login novamente');
    } 
  } else {
    Deslogar();
    throw new Error('Sua sessão expirou faça o login novamente');
  } 
}
/* Abre módulos do sistema */
const AbrePagina = (url) => router.push(url);
/* Remove acentuação */
const removeAccents = (strAccents) => {
  var strAccents = strAccents.split("");
  var strAccentsOut = new Array();
  var strAccentsLen = strAccents.length;
  var accents = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
  var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
  for (var y = 0; y < strAccentsLen; y++) {
    accents.indexOf(strAccents[y]) != -1 ? (strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1)) : (strAccentsOut[y] = strAccents[y]);
  }
  strAccentsOut = strAccentsOut.join("");
  return strAccentsOut;
}
/*Converte data padrão americando para brasileiro. (DD/MM/YYYY) somente  */
const dateBR = (value) => {
  if (!value) return 'Data Inválida';
  return moment(String(value)).format('DD/MM/YYYY')
}
/*Converte data padrão americando para brasileiro. (DD/MM/YYYY H:i:s) somente  */
const datetimeBR = (value) => {
  if (!value) return 'Data Inválida';
  return moment(String(value)).format('DD/MM/YYYY HH:mm:ss')
}
export { checaValidadeToken, Deslogar, ValidaToken, DeslogarSilencioso, Derrubar, AbrePagina, removeAccents, dateBR, datetimeBR, FixToast }