import Swal from 'sweetalert2'
/* Aviso de erro */
const AvisoErro = (mensagem) => {
  Swal.fire({
    title: 'Aviso',
    html: mensagem,
    icon: 'error',
    allowOutsideClick: false,
    confirmButtonColor: '#5C2E91',
    confirmButtonText: 'Fechar',
    focusConfirm:true,
  });
}
/* Aviso de erro */
const AvisoValidacaoPromise = (mensagem) => {
  return new Promise((resolve) => {
    Swal.fire({
      title: 'Aviso',
      html: mensagem,
      icon: 'error',
      allowOutsideClick: false,
      confirmButtonColor: '#5C2E91',
      confirmButtonText: 'Fechar',
      focusConfirm:true,
    }).then((result) => {
      resolve(result);
    });
  });
}
/* Aviso de erro */
const AvisoErroPromise = (mensagem) => {
  return new Promise((resolve) => {
    Swal.fire({
      title: 'Aviso',
      html: mensagem,
      icon: 'error',
      allowOutsideClick: false,
      confirmButtonColor: '#5C2E91',
      confirmButtonText: 'Fechar',
      focusConfirm:true,
    }).then((result) => {
      resolve(result);
    });
  });
}
/* Aviso de confirmação */
const AvisoSucesso = (mensagem) => {
  Swal.fire({
    title: 'Confirmação',
    html: mensagem,
    icon: 'success',
    confirmButtonColor: '#5C2E91',
    allowOutsideClick: false,
    confirmButtonText: 'Fechar',
    focusConfirm:true,
  });
}
/* Aviso de confirmação com retorno */
const AvisoSucessoPromise = (mensagem) => {
  return new Promise((resolve) => {
    Swal.fire({
      title: 'Confirmação',
      html: mensagem,
      icon: 'success',
      confirmButtonColor: '#5C2E91',
      confirmButtonText: 'Fechar',
      allowOutsideClick: false,
      focusConfirm:true,
    }).then((result) => {
      resolve(result);
    });
  });
}
/* Pergunta */
const Pergunta = (pergunta) => {
  return new Promise((resolve) => {
    Swal.fire({
      title: 'Confirmação',
      html: pergunta,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#5C2E91',
      confirmButtonText: `Sim`,
      cancelButtonText: `Não`,
      focusConfirm:true,
      allowOutsideClick: false,
    }).then((result) => {
      resolve(result);
    });
  });
}
export { AvisoErro, AvisoErroPromise, AvisoSucesso, Pergunta, AvisoValidacaoPromise, AvisoSucessoPromise }