import Vue from 'vue'
import VueRouter from 'vue-router'
import autenticacao from "../middlewares/autenticar";
Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'inicio',
    beforeEnter: autenticacao,
  },
  {
    path: '/login',
    name: 'login',
    meta: { transition: 'fade-in-up' },
    component: () => import('../views/Login.vue')
  },
  {
    path: '/recuperacao',
    name: 'recuperacao',
    meta: { transition: 'fade-in-down' },
    component: () => import('../views/Recuperacao.vue')
  },
  {
    path: '/recuperar/:token/:email',
    name: 'alterar-senha',
    meta: { transition: 'fade-in-down' },
    component: () => import('../views/AlterarSenha.vue')
  },
  {
    path: '/ativar/:token',
    name: 'ativacao',
    meta: { transition: 'fade-in-down' },
    component: () => import('../views/Ativacao.vue')
  },
  {
    path: '/agendar',
    name: 'agendar',
    beforeEnter: autenticacao,
    meta: { transition: 'fade-in-up' },
    component: () => import('../views/InicioUsuario.vue')
  },
]

const router = new VueRouter({
  mode: "history",
  routes
});
export default router;