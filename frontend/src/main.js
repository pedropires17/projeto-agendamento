import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router'
import VuePageTransition from 'vue-page-transition'
import VueSweetalert2 from 'vue-sweetalert2';
import {  AvisoErro, AvisoErroPromise, AvisoSucesso, Pergunta, AvisoValidacaoPromise, AvisoSucessoPromise } from "./middlewares/avisos";
import { ValidaToken, AbrePagina, datetimeBR, dateBR } from './middlewares/funcoes';
import axios from 'axios'
import VueAxios from 'vue-axios'
import axiosRetry from 'axios-retry';
import VueOnlineProp from "vue-online-prop"
axiosRetry(axios, { retries: 30 });
//Importa Avisos para o sistema e componentes
Vue.mixin({
  filters: { datetimeBR, dateBR },
  methods: { AvisoErro, AvisoErroPromise, AvisoSucesso, AvisoValidacaoPromise, AvisoSucessoPromise, Pergunta, ValidaToken, AbrePagina }
});
Vue.use(VueOnlineProp)
Vue.use(VueSweetalert2);
Vue.use(VuePageTransition)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)
//Só injeta o bearer se forem rquisições na URL da API
axios.interceptors.request.use(function (config) {
  //Valida o token em todas as requisições exceto login
  if (!config.url.includes("ativar") 
      && !config.url.includes("autenticar") 
      && !config.url.includes("moradores/insert") 
      && !config.url.includes("recuperar") 
      && !config.url.includes("recuperacao")) ValidaToken();
  //Injeta o bearer na requisição para a api apenas
  if (config.url.includes(process.env.VUE_APP_API_BASEURL)){
    config.headers = { 'Accept': 'application/json; charset=utf-8', 
                       Authorization: "Bearer " + localStorage.getItem("userToken") };
  }
  return config;
});
//Valida token nas respostas da API
axios.interceptors.response.use(
  (response) => {
    //Se retornou novo token injeta no storage
    if (response.headers.authorization) localStorage.setItem("userToken",response.headers.authorization.replace("Bearer ", ""));
    return response;
  },
  (error) => {
    //Se existir loading apaga
    // if(localStorage.getItem("showLoading")) localStorage.removeItem('showLoading');
    //Se for erro da aplicação na URL da API
    if (error.response && error.response.config.url.includes(process.env.VUE_APP_API_BASEURL)){
      //Mensagem de erro de sessão / token (401)
      if (error.response.status === 401) AvisoErroPromise(error.response.data.mensagem).then(() => Derrubar());
      //Mensagens de erros de validações (400)
      else if (error.response.status === 400) AvisoErro(error.response.data.mensagem);
      //Mensagem de erro sem permissão (403)
      else if (error.response.status === 403) AbrePagina('/permissao');
      //Mensagem de erro interno (500)
      else if (error.response.status === 500) AvisoErro('Erro interno tente novamente mais tarde');
      //Outros erros
      else  return Promise.reject(error);
    //Outros erros
    } else  return Promise.reject(error);
  }
);
Vue.config.productionTip = true

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
