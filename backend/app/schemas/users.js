var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js'),
    bcrypt = require('bcrypt');
// Schema > Users
const Users = sequelize.define('tblusers', {
  strname: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  struser: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  strpassword: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  stremail: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true,
      isEmail : { msg: 'E-mail inválido' }
    }
  },
  strphone: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  strpermissions: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  struserinsert: {
    type: Sequelize.STRING
  },
  struserupdate: {
    type: Sequelize.STRING
  },
  boolactive: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
  booldeleted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  boolnotifications: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
    validate: {
      notEmpty: true
    }
  },
}, {
  instanceMethods: {
    authenticate: function (value) {
      return (bcrypt.compareSync(value, this.strpassword)) ? this : false;
    }
  }
});
// Sincronize > Table > Users
Users.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S') console.log('Tabela de usuários sincronizada com sucesso!');
  Users.count({ where: {struser: 'demo' } }).then(function(contador){
    if (contador == 0) {
      Users.create({
        strname: 'Demonstração',
        struser: 'demo',
        strpassword: 'demoadm',
        stremail:'demo@demo.com.br',
        strphone:'(00)00000-0000',
        boolaactive: true,
        booldeleted: false,
        boolnotifications: true,
      });
    }
  });
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S') console.log('Erro ao criar tabela de usuários');
});
//Criptografa > Senha > Antes de Cadastrar
Users.beforeCreate((usuario) => {
  return bcrypt.hash(usuario.strpassword, 10)
    .then(hash => {
      usuario.strpassword = hash;
    })
    .catch(err => {
      throw new Error();
    });
});
//Criptografa > Senha > Antes de Alterar
Users.beforeBulkUpdate((usuario) => {
  if(usuario.attributes.strpassword){
    return bcrypt.hash(usuario.attributes.strpassword, 10)
      .then(hash => {
        usuario.attributes.strpassword = hash;
      })
      .catch(err => {
        throw new Error();
      });
  }
});
module.exports = { Users }