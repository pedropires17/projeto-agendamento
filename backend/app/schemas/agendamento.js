var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > Agendamentos
const Agendamentos = sequelize.define('tblagendamentos', {
  id: {
    type: Sequelize.INTEGER(11).UNSIGNED.ZEROFILL,
    primaryKey: true,
    autoIncrement: true
  },
  intdtadisponivelid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você selecionou a Data de agendamento'
      },
      min : {
        args: [1],
        msg: 'Você não selecionou a Data de agendamento'
      }
    }
  },
  strturno: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você selecionou o Turno'
      },
      min : {
        args: [1],
        msg: 'Você não selecionou o Turno'
      }
    }
  },
  intmoradorid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você não selecionou o morador'
      },
      min : {
        args: [1],
        msg: 'Você não selecionou o morador'
      }
    }
  },
  strip: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  struserinsert: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  struserupdate: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: false
    }
  },
  boolactive: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
  booldeleted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
});
// Sincronize > Table > Agendamentos
Agendamentos.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de agendamentos sincronizada com sucesso!');
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de agendamentos');
});
Agendamentos.afterCreate(async agendamento => {
  const Agendamento = require('../controllers/agendamento.js');
  return await Agendamento.EnviarEmail(agendamento);
})
module.exports = { Agendamentos }