var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > Calendario
const Calendario = sequelize.define('tblcalendario', {
  dtadisponivel:{
    type: Sequelize.DATEONLY,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo Data'
      },
      isDate: {
        msg: 'Data Inválida'
      }
    }
  },
  intqtd: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo quantidade'
      },
      min : {
        args: [1],
        msg: 'O campo quantidade deve conter no mínimo 1 dia'
      }
    }
  },
  intqtdmatutino: {
    type: Sequelize.INTEGER,
  },
  intqtdvespertino: {
    type: Sequelize.INTEGER,
  },
  struserinsert: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  struserupdate: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: false
    }
  },
  boolactive: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
  booldeleted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
});
// Sincronize > Table > Calendario
Calendario.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de calendário sincronizada com sucesso!');
  Calendario.destroy({
    where: {},
    truncate: true
  }).then(async ()=>{
    await Calendario.create({
      dtadisponivel: '2021-02-22',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-02-23',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-02-24',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-02-25',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-02-26',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-01',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-02',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-03',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-04',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-05',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-08',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-09',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-10',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-11',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-12',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-15',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-16',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-17',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-18',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-19',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-22',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-23',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-24',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-25',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-26',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-29',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-30',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
    await Calendario.create({
      dtadisponivel: '2021-03-31',
      intqtd: 20,
      intqtdmatutino:10,
      intqtdvespertino:10,
      struserinsert: 'sistema',
      boolactive: true
    });
  });
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de calendário');
});
module.exports = { Calendario }