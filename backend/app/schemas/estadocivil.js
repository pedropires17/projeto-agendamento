var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > Estado Civil
const EstadoCivil = sequelize.define('tblestadocivil', {
  strnome: {
    type: Sequelize.STRING,
  },
  boolativo: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
});
// Sincronize > Table > Estado Civil
EstadoCivil.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de estado cívil sincronizada com sucesso!');
  EstadoCivil.destroy({
    where: {},
    truncate: true
  }).then(async()=>{
    await EstadoCivil.create({
      strnome: 'SOLTEIRO (A)',
      boolativo: true
    });
    await EstadoCivil.create({
      strnome: 'CASADO (A)',
      boolativo: true
    });
    await EstadoCivil.create({
      strnome: 'VIUVO (A)',
      boolativo: true
    });
    await EstadoCivil.create({
      strnome: 'DIVORCIADO (A)',
      boolativo: true
    });
});
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de estado cívil');
});
module.exports = { EstadoCivil }