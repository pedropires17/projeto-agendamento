var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > Logs
const Logs = sequelize.define('tbllogs', {
  intjobid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: true
    }
  },
  strcurlcommand: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  strrequest: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  strresponse: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  timerequest: {
    type: Sequelize.DATE,
    validate: {
      notEmpty: true
    }
  },
  timeresponse: {
    type: Sequelize.DATE,
    validate: {
      notEmpty: true
    }
  },
});
// Sincronize > Table > Logs
Logs.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de logs sincronizada com sucesso!');
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de logs');
});
module.exports = { Logs }