var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > E-mails Enviados
const Emails = sequelize.define('tblemails', {
  strremetente: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  strdestino: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  strassunto: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  stmensagem: {
    type: Sequelize.TEXT,
    validate: {
      notEmpty: true
    }
  },
  smtpresponse: {
    type: Sequelize.DATE,
    validate: {
      notEmpty: true
    }
  },
  boolenviado: {
    type: Sequelize.BOOLEAN,
    validate: {
      notEmpty: true
    }
  },

});
// Sincronize > Table > E-mails
Emails.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de e-mails sincronizada com sucesso!');
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de e-mails');
});
module.exports = { Emails }