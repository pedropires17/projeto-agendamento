var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js');
// Schema > Ativação
const Ativacoes = sequelize.define('tblativacoes', {
  intmoradorid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você selecionou o morador'
      },
      min : {
        args: [1],
        msg: 'Você selecionou o morador'
      }
    }
  },
  strcodigo: {
    allowNull: false,
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1
  },
  inttipoid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você selecionou o tipo'
      },
      min : {
        args: [1],
        msg: 'Você selecionou o tipo'
      }
    },
    defaultValue: 2
  },
  strip: {
    type: Sequelize.STRING,
  },
  boolused: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  boolexpire: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
});
// Sincronize > Table > Ativação
Ativacoes.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de ativações sincronizada com sucesso!');
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de ativações');
});
Ativacoes.afterCreate(async ativacao => {
  const Ativacao = require('../controllers/ativacao.js');
  return await Ativacao.EnviarEmail(ativacao);
})
module.exports = { Ativacoes }