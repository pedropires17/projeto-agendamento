var Sequelize = require('sequelize'),
    sequelize = require('../../config/db.js'),
    bcrypt = require('bcrypt');
// Schema > Moradores
const Moradores = sequelize.define('tblmoradores', {
  strnome: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo nome completo'
      }
    }
  },
  strcelular: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo Celular'
      }
    }
  },
  stremail: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo e-mail'
      },
      isEmail:{
        msg: 'E-mail inválido'
      }
    }
  },
  strsenha: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo senha'
      },
      len: {
        args: [6,12],
        msg: 'Sua senha deve conter no mín. 6 e máx. 12 caracteres'
      }
    }
  },
  strcpf: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo cpf'
      }
    }
  },
  strrg: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: {
        msg: 'Você não preencheu o campo rg'
      }
    }
  },
  intestadocivilid: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: {
        msg: 'Você não selecionou o estado cívil'
      },
      min: {
        args:[1],
        msg: 'Você não selecionou o estado cívil'
      }
    }
  },
  struserinsert: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  struserupdate: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: false
    }
  },
  boolactive: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  booldeleted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
}, {
  instanceMethods: {
    authenticate: function (value) {
      return (bcrypt.compareSync(value, this.strsenha)) ? this : false;
    }
  }
});
// Sincronize > Table > Moradores
Moradores.sync()
.then(() => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Tabela de moradores sincronizada com sucesso!');
})
.catch(err => {
  if(process.env.APP_ORM_DEBUG == 'S')  console.log('Erro ao criar tabela de moradores');
});
//Criptografa > Senha > Antes de Cadastrar
Moradores.beforeCreate(async(morador) => {
  if(morador.stremail) morador.stremail = morador.stremail.toLowerCase();
  if(morador.strnome) morador.strnome = morador.strnome.toUpperCase();
  return bcrypt.hash(morador.strsenha, 10)
    .then(hash => {
      morador.strsenha = hash;
    })
    .catch(err => {
      throw new Error();
    });
});
//Criptografa > Senha > Antes de Alterar
Moradores.beforeBulkUpdate((morador) => {
 if(morador.stremail) morador.stremail = morador.stremail.toLowerCase();
 if(morador.strnome) morador.strnome = morador.strnome.toUpperCase();
  if(morador.attributes.strsenha){
    return bcrypt.hash(morador.attributes.strsenha, 10)
      .then(hash => {
        morador.attributes.strsenha = hash;
      })
      .catch(err => {
        throw new Error();
      });
  }
});
//Envia e-mail de ativação
Moradores.afterCreate(async(morador) => {
  const Moradores = require('../controllers/moradores.js');
  await Moradores.EnviarEmail(morador);
});
module.exports = { Moradores }