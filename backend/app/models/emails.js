var SchemaEmails = require('../schemas/emails.js');
module.exports = {
  // Model > E-mails > Insert
  async createEmail(obj){
    return await SchemaEmails.Emails.create(obj);
  },
  // Model > E-mails > Update
  async updateEmail(obj,id){
    return await SchemaEmails.Emails.update(obj,{ where: { id } });
  },
  // Model > E-mails > List All
  async getAllEmail(){
    return await SchemaEmails.Emails.findAll();
  },
  // Model > E-mails > List > Indiviual
  async getEmail(obj){
    return await SchemaEmails.Emails.findOne({ where: obj });
  },
}