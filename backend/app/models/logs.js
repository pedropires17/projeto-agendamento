var SchemaLogs = require('../schemas/logs.js');
module.exports = {
  // Model > Logs > Cadastrar
  async createLog({
    req
  }){
    return await SchemaLogs.Logs.create({
     req
    });
  },
  // Model > Logs > Alterar
  async updateLog({
    req
  }){
    return await SchemaLogs.Logs.update({
        req
    }, {
      where: { id }
    });
  },
  // Model > Logs > Ativar
  async updateJobActive({
    req,
    id
  }){
    return await SchemaLogs.Logs.update({
        boolactive: true,
        struserupdate: req
    }, {
      where: { id }
    });
  },
  // Model > Logs > Inativar
  async updateJobInactive({
         req,
         id
        }){
        return await SchemaLogs.Logs.update({
            boolactive: false,
            struserupdate: req
        }, {
          where: { id }
        });
      },
  // Model > Logs > Apagar
  async deleteLog({
    req,
    id
  }) {
    return await SchemaLogs.Logs.update({
      booldeleted: true,
      struserupdate: req
    }, {
      where: { id }
    });
  },
  // Model > Logs > Listar Todos
  async getAllLogs(){
    return await SchemaLogs.Logs.findAll({
        where: obj
    });
  },
  // Model > Logs > Indiviual
  async getLog(obj){
    return await SchemaLogs.Logs.findOne({
      where: obj
    });
  },
}