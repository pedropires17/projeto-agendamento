var SchemaAgendamento = require('../schemas/agendamento.js');
module.exports = {
  // Model > Agendamento > Insert
  async createAgendamento(obj){
    return await SchemaAgendamento.Agendamentos.create(obj);
  },
  // Model > Agendamento > Update
  async updateAgendamento(obj,id){
    return await SchemaAgendamento.Agendamentos.update(obj,{ where: { id } });
  },
  // Model > Agendamento > List All
  async getAllAgendamento(){
    return await SchemaAgendamento.Agendamentos.findAll({
        where: { 
            booldeleted: false
        }
    });
  },  
  // Model > Agendamento > List All > User logged
  async getAllMeusAgendamentos(id){
    const { QueryTypes } = require('sequelize');
    const db = require('../../config/db');
    return await db.query(`SELECT tblagendamentos.id,
                                  tblagendamentos.createdAt,
                                  tblagendamentos.strturno,
                                  tblmoradores.strnome,
                                  tblcalendarios.dtadisponivel 
                           FROM tblagendamentos
                           INNER JOIN tblmoradores ON
                           (tblmoradores.id = tblagendamentos.intmoradorid) 
                           INNER JOIN tblcalendarios ON
                           (tblcalendarios.id = tblagendamentos.intdtadisponivelid)
                           WHERE tblagendamentos.intmoradorid = ?
                           AND tblagendamentos.booldeleted = false
                           ORDER BY tblcalendarios.dtadisponivel DESC`, { replacements: [id],type: QueryTypes.SELECT});
  },
  // Model > Agendamento > List > Indiviual
  async getAgendamento(obj){
    return await SchemaAgendamento.Agendamentos.findOne({ where: obj });
  },
}