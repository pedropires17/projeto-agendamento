var SchemaAtivacoes = require('../schemas/ativacao.js');
module.exports = {
  // Model > Ativações > Insert
  async createAtivacao(obj){
    return await SchemaAtivacoes.Ativacoes.create(obj);
  },
  // Model > Ativações > Update
  async updateAtivacao(obj,id){
    return await SchemaAtivacoes.Ativacoes.update(obj,{ where: { id } });
  },
  // Model > Ativações > List All
  async getAllAtivacao(){
    return await SchemaAtivacoes.Ativacoes.findAll({
        where: { 
            booldeleted: false
        }
    });
  },
  // Model > Ativações > List > Indiviual
  async getAtivacao(obj){
    return await SchemaAtivacoes.Ativacoes.findOne({ where: obj });
  },
}