var SchemaUsers = require('../schemas/users.js');
module.exports = {
  // Model > Users > Insert
  async createUser(obj){
    return await SchemaUsers.Users.create(obj);
  },
  // Model > Users > Update
  async updateUser(obj, id){
    return await SchemaUsers.Users.update(obj,{ where: { id } });
  },
  // Model > Users > List All
  async getAllUsers(){
    return await SchemaUsers.Users.findAll({
      attributes: {
        exclude: ['strpassword']
      },
      where: {
          booldeleted: false
      }
    });
  },
  // Model > Users > List > Indiviual
  async getUser(obj){
    return await SchemaUsers.Users.findOne({ where: obj });
  },
    // Model > Users > List > JWT
    async getUserJWT(obj){
      return await SchemaUsers.Users.findOne({
        attributes: ['id','struser','strpermissions'],
        where: obj,
      });
    },
  // Model > Users > List > Authentication
  async getAuthUser(obj){
    return await SchemaUsers.Users.findOne({
      attributes: ['id','strname','struser','strpassword','stremail','strphone','strpermissions'],
      where: obj,
    });
  },
}