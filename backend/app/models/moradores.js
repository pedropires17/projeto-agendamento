var SchemaMoradores = require('../schemas/moradores.js');
module.exports = {
  // Model > Moradores > Insert
  async createMoradores(obj){
    return await SchemaMoradores.Moradores.create(obj);
  },
  // Model > Moradores > Update
  async updateMoradores(obj,id){
    return await SchemaMoradores.Moradores.update(obj,{ where: { id } });
  },
  // Model > Moradores > Update
  async updateMoradoresAtivacao(obj,id){
    console.log(obj,{ where: id });
    return await SchemaMoradores.Moradores.update(obj,{ where: id });
  },
  // Model > Moradores > List All
  async getAllMoradores(){
    return await SchemaMoradores.Moradores.findAll({
        where: { 
            booldeleted: false
        }
    });
  },
  // Model > Users > List > JWT
  async getMoradorJWT(obj){
    return await SchemaMoradores.Moradores.findOne({
      attributes: ['id','strnome','intestadocivilid'],
      where: obj,
    });
  },
  // Model > Moradores > List > Indiviual
  async getMoradores(obj){
    return await SchemaMoradores.Moradores.findOne({ where: obj });
  },
}