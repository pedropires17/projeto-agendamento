var SchemaCalendario = require('../schemas/calendario.js');
module.exports = {
  // Model > Calendário > Insert
  async createCalendario(obj){
    return await SchemaCalendario.Calendario.create(obj);
  },
  // Model > Calendário > Update
  async updateCalendario(obj,id){
    return await SchemaCalendario.Calendario.update(obj,{ where: { id } });
  },
  // Model > Calendário > List All
  async getAllCalendario(){
    return await SchemaCalendario.Calendario.findAll({
        where: { 
            booldeleted: false
        }
    });
  },
  // Model > Calendário > Lista Dias Disponiveis
  async getAllDisponiveis(){
    const { QueryTypes } = require('sequelize');
    const db = require('../../config/db');
    return await db.query(`SELECT tblcalendarios.id,
                                  tblcalendarios.dtadisponivel,
                                  tblcalendarios.intqtd as qtd,
                                  (tblcalendarios.intqtdmatutino - (SELECT COUNT(*) 
                                   FROM tblagendamentos
                                   WHERE tblagendamentos.intdtadisponivelid = tblcalendarios.id
                                   AND tblagendamentos.strturno = 'M')) as qtd_matutino,
                                   (tblcalendarios.intqtdvespertino -(SELECT COUNT(*) 
                                   FROM tblagendamentos
                                   WHERE tblagendamentos.intdtadisponivelid = tblcalendarios.id
                                   AND tblagendamentos.strturno = 'V')) as qtd_vespertino
                           FROM tblcalendarios
                           WHERE tblcalendarios.intqtd > (SELECT COUNT(*) 
                                                          FROM tblagendamentos
                                                          WHERE tblagendamentos.intdtadisponivelid = tblcalendarios.id)
                           AND tblcalendarios.booldeleted = false
                           AND tblcalendarios.dtadisponivel > NOW()
                           ORDER BY dtadisponivel ASC`, { type: QueryTypes.SELECT});
  },
  // Model > Calendário > List > Indiviual
  async getCalendario(obj){
    return await SchemaCalendario.Calendario.findOne({ where: obj });
  },
}