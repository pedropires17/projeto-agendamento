var SchemaEstadoCivil = require('../schemas/estadocivil.js');
module.exports = {
  // Model > Estado Civil > List All
  async getAllEstadoCivil(){
    return await SchemaEstadoCivil.EstadoCivil.findAll({
        where: { 
            booldeleted: false
        }
    });
  },
}