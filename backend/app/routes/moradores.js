module.exports = (routes) => {
   /**
   * @swagger
   *
   * /moradores/insert:
   *   post:
   *     description: Insere um Morador
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: formData
   *         name: strnome
   *         description: Nome do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: strcpf
   *         description: CPF do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: strrg
   *         description: RG do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: stremail
   *         description: E-mail do Morador
   *         required: true
   *         type: string
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Morador cadastrado com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Moradores > Insert
   routes.post(process.env.APP_BASE_URL+'/moradores/insert', 
              (req,res) => routes.controllers.moradores.Create(routes, req, res)
   );
   /**
   * @swagger
   *
   * /moradores/update/{id}:
   *   put:
   *     description: Altera um Morador
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id do morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: strnome
   *         description: Nome do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: strcpf
   *         description: CPF do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: strrg
   *         description: RG do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: stremail
   *         description: E-mail do Morador
   *         required: true
   *         type: string
   *       - in: formData
   *         name: boolactive
   *         description: Ativa / Inativa um morador
   *         required: true
   *         type: boolean
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Dado(s) alterado(s) com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Moradores > Update
   routes.put(process.env.APP_BASE_URL+'/moradores/update/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.moradores.Update(routes, req, res)
   );
   /**
   * @swagger
   *
   * /moradores/delete/{id}:
   *   delete:
   *     description: Apaga um Morador
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id do Morador
   *         required: true
   *         type: string
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Morador apagado com sucesso!
   *       422:
   *         description: Morador não encontrado!
   */
   // Route > Moradores > Update
   routes.delete(process.env.APP_BASE_URL+'/moradores/delete/:id', 
                 passport.authenticate('jwt', { session: false }), 
                 (req,res) => routes.controllers.moradores.Delete(routes, req, res)
   );
   /**
   * @swagger
   *
   * /moradores/recover/{id}:
   *   put:
   *     description: Recuperar um Morador
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id do Morador
   *         required: true
   *         type: string
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Morador recuperado com sucesso!
   *       422:
   *         description: Morador não encontrado!
   */
   // Route > Moradores > Recover
   routes.put(process.env.APP_BASE_URL+'/moradores/recover/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.moradores.Recover(routes, req, res)
   );
   /**
   * @swagger
   *
   * /moradores/list:
   *   get:
   *     description: Lista todos os moradores
   *     produces:
   *       - application/json
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações dos Moradores
   *       422:
   *         description: Erro ao listar Moradores!
   */
   // Route > Moradores > List
   routes.get(process.env.APP_BASE_URL+'/moradores/list', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.moradores.List(routes, req, res)
   );
   /**
   * @swagger
   *
   * /moradores/info/{id}:
   *   get:
   *     description: Informações de um Morador especifico
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id do Morador
   *         required: true
   *         type: string
   *     tags:
   *      - Moradores
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações do Morador
   *       422:
   *         description: Erro ao listar Moradores!
   */
   // Route > Moradores > Info
   routes.get(process.env.APP_BASE_URL+'/moradores/info/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.moradores.Read(routes, req, res)
   );
}