module.exports = (routes) => {
   /**
   * @swagger
   *
   * /calendario/insert:
   *   post:
   *     description: Insere uma Data disponível
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: formData
   *         name: dtadisponivel
   *         description: Data do agendamento
   *         required: true
   *         type: string
   *         format: date
   *       - in: formData
   *         name: intqtd
   *         description: Quantidade de vagas disponiveis
   *         required: true
   *         type: string
   *       - in: formData
   *         name: boolactive
   *         description: Ativa / Inativa uma disponibilidade
   *         required: true
   *         type: boolean
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data cadastrada com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Calendário > Insert
   routes.post(process.env.APP_BASE_URL+'/calendario/insert', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.Create(routes, req, res)
   );
   /**
   * @swagger
   *
   * /calendario/update/{id}:
   *   put:
   *     description: Altera uma Data de disponibilidade
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da data
   *         required: true
   *         type: string
   *       - in: formData
   *         name: dtadisponivel
   *         description: Data do agendamento
   *         required: true
   *         type: string
   *         format: date
   *       - in: formData
   *         name: intqtd
   *         description: Quantidade de vagas disponiveis
   *         required: true
   *         type: string
   *       - in: formData
   *         name: boolactive
   *         description: Ativa / Inativa uma disponibilidade
   *         required: true
   *         type: boolean
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Dado(s) alterado(s) com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Calendário > Update
   routes.put(process.env.APP_BASE_URL+'/calendario/update/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.Update(routes, req, res)
   );
   /**
   * @swagger
   *
   * /calendario/delete/{id}:
   *   delete:
   *     description: Apaga uma data disponível
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da data
   *         required: true
   *         type: string
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data apagada com sucesso!
   *       422:
   *         description: Data não encontrada!
   */
   // Route > Calendário > Update
   routes.delete(process.env.APP_BASE_URL+'/calendario/delete/:id', 
                 passport.authenticate('jwt', { session: false }), 
                 (req,res) => routes.controllers.calendario.Delete(routes, req, res)
   );
   /**
   * @swagger
   *
   * /calendario/recover/{id}:
   *   put:
   *     description: Recuperar uma Data de disponibilidade
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da Data
   *         required: true
   *         type: string
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data recuperada com sucesso!
   *       422:
   *         description: Data não encontrada!
   */
   // Route > Calendário > Recover
   routes.put(process.env.APP_BASE_URL+'/calendario/recover/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.Recover(routes, req, res)
   );
   /**
   * @swagger
   *
   * /calendario/list:
   *   get:
   *     description: Lista todas as datas
   *     produces:
   *       - application/json
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações das Datas
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Calendário > List
   routes.get(process.env.APP_BASE_URL+'/calendario/list', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.List(routes, req, res)
   );
      /**
   * @swagger
   *
   * /calendario/disponivel:
   *   get:
   *     description: Lista todas as datas disponiveis para agendamento
   *     produces:
   *       - application/json
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações das Datas
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Calendário > Lista Disponibilidade
   routes.get(process.env.APP_BASE_URL+'/calendario/disponivel', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.ListDisponivel(routes, req, res)
   );
   /**
   * @swagger
   *
   * /calendario/info/{id}:
   *   get:
   *     description: Informações de uma Data específica
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da Data
   *         required: true
   *         type: string
   *     tags:
   *      - Calendário
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações da Data
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Calendário > Info
   routes.get(process.env.APP_BASE_URL+'/calendario/info/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.calendario.Read(routes, req, res)
   );
}