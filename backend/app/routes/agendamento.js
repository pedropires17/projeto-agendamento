module.exports = (routes) => {
   /**
   * @swagger
   *
   * /agendamentos/insert:
   *   post:
   *     description: Insere uma Data disponível
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: formData
   *         name: intdtadisponivelid
   *         description: Data do agendamento
   *         required: true
   *         type: integer
   *       - in: formData
   *         name: intmoradorid
   *         description: Quantidade de vagas disponiveis
   *         required: true
   *         type: integer
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data cadastrada com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Agendamentos > Insert
   routes.post(process.env.APP_BASE_URL+'/agendamentos/insert', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.Create(routes, req, res)
   );
   /**
   * @swagger
   *
   * /agendamentos/update/{id}:
   *   put:
   *     description: Altera uma Data de disponibilidade
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da data
   *         required: true
   *         type: string
   *       - in: formData
   *         name: dtadisponivel
   *         description: Data do agendamento
   *         required: true
   *         type: date
   *       - in: formData
   *         name: intqtd
   *         description: Quantidade de vagas disponiveis
   *         required: true
   *         type: string
   *       - in: formData
   *         name: boolactive
   *         description: Ativa / Inativa uma disponibilidade
   *         required: true
   *         type: boolean
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Dado(s) alterado(s) com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Agendamentos > Update
   routes.put(process.env.APP_BASE_URL+'/agendamentos/update/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.Update(routes, req, res)
   );
   /**
   * @swagger
   *
   * /agendamentos/delete/{id}:
   *   delete:
   *     description: Apaga uma data disponível
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da data
   *         required: true
   *         type: string
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data apagada com sucesso!
   *       422:
   *         description: Data não encontrada!
   */
   // Route > Agendamentos > Update
   routes.delete(process.env.APP_BASE_URL+'/agendamentos/delete/:id', 
                 passport.authenticate('jwt', { session: false }), 
                 (req,res) => routes.controllers.agendamento.Delete(routes, req, res)
   );
   /**
   * @swagger
   *
   * /agendamentos/recover/{id}:
   *   put:
   *     description: Recuperar uma Data de disponibilidade
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da Data
   *         required: true
   *         type: string
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Data recuperada com sucesso!
   *       422:
   *         description: Data não encontrada!
   */
   // Route > Agendamentos > Recover
   routes.put(process.env.APP_BASE_URL+'/agendamentos/recover/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.Recover(routes, req, res)
   );
   /**
   * @swagger
   *
   * /agendamentos/list:
   *   get:
   *     description: Lista todas as datas disponiveis
   *     produces:
   *       - application/json
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações das Datas
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Agendamentos > List
   routes.get(process.env.APP_BASE_URL+'/agendamentos/list', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.List(routes, req, res)
   );
    /**
   * @swagger
   *
   * /agendamentos/meus-agendamentos:
   *   get:
   *     description: Lista todos agendamentos do usuário logado
   *     produces:
   *       - application/json
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações das Datas
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Agendamentos > List
   routes.get(process.env.APP_BASE_URL+'/agendamentos/meus-agendamentos', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.ListMeusAgendamentos(routes, req, res)
   );
   /**
   * @swagger
   *
   * /agendamentos/info/{id}:
   *   get:
   *     description: Informações de uma Data específica
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: path
   *         name: id
   *         description: Id da Data
   *         required: true
   *         type: string
   *     tags:
   *      - Agendamentos
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Mostra informações da Data
   *       422:
   *         description: Erro ao listar Calendário!
   */
   // Route > Agendamentos > Info
   routes.get(process.env.APP_BASE_URL+'/agendamentos/info/:id', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.agendamento.Read(routes, req, res)
   );
}