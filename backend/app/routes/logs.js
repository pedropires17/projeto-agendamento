module.exports = (routes) => {
    /**
    * @swagger
    *
    * /logs/list:
    *   get:
   *     security:
   *      - Bearer: []
    *     description: Lista logs da ultima semana
    *     produces:
    *       - application/json
    *     tags:
    *      - Logs
    *     responses:
    *       200:
    *         description: Lista as informações 
    *                      strcurlcommand, strrequest, strresponse, createdAt, updatedAt
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Logs > List
    routes.get(process.env.APP_BASE_URL+'/logs/list', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.logs.List(routes, req, res)
    );
 }