module.exports = (routes) => {
    /**
    * @swagger
    *
    * /autenticar:
    *   post:
    *     description: Loga usuário Administrador
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: struser
    *         description: Usuário
    *         required: true
    *         type: string
    *         in: formData
    *       - name: strpassword
    *         description: Senha
    *         required: true
    *         type: string
    *         format: password
    *         in: formData
    *     tags:
    *      - Autenticação
    *     responses:
    *       200:
    *         description: Retorna JSON webtoken
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Authentication
    routes.post(process.env.APP_BASE_URL+'/autenticar', 
        (req,res) => routes.controllers.authentication.Login(routes, req, res)
    );
    /**
    * @swagger
    *
    * /autenticar-morador:
    *   post:
    *     description: Loga usuário Morador
    *     produces:
    *       - application/json
    *     parameters:
    *       - name: stremail
    *         description: E-mail
    *         required: true
    *         type: string
    *         in: formData
    *       - name: strsenha
    *         description: Senha
    *         required: true
    *         type: string
    *         format: password
    *         in: formData
    *     tags:
    *      - Autenticação
    *     responses:
    *       200:
    *         description: Retorna JSON webtoken
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Authentication
    routes.post(process.env.APP_BASE_URL+'/autenticar-morador', 
        (req,res) => routes.controllers.authentication.LoginMorador(routes, req, res)
    );
 }