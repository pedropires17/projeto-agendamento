module.exports = (routes) => {
   /**
   * @swagger
   *
   * /ativacao/insert:
   *   post:
   *     description: Insere uma Data disponível
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: formData
   *         name: intmoradorid
   *         description: ID do morador
   *         required: true
   *         type: integer
   *       - in: formData
   *         name: inttipoid
   *         description: ID do tipo (1 = Recuperação de Senha e 2 = Ativação de Conta)
   *         required: true
   *         type: integer
   *     tags:
   *      - Ativações / Recuperações
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Ativação/Recuperação cadastrada com sucesso!
   *       422:
   *         description: Dado(s) incorreto(s)
   */
   // Route > Ativação > Insert
   routes.post(process.env.APP_BASE_URL+'/ativacao/insert', 
              passport.authenticate('jwt', { session: false }), 
              (req,res) => routes.controllers.ativacao.Create(routes, req, res)
   );
   /**
   * @swagger
   *
   * /ativacao/ativar:
   *   put:
   *     description: Altera uma Data de disponibilidade
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: formData
   *         name: strtoken
   *         description: token de ativacao
   *         required: true
   *         type: date
   *     tags:
   *      - Ativações / Recuperações
   *     security:
   *      - Bearer: []
   *     responses:
   *       200:
   *         description: Parabéns! Sua conta foi ativada com sucesso e você já pode utilizar o sistema.
   *       422:
   *         description: O Token informado é inválido!
   *                      O Token informado já foi utilizado!
   */
   // Route > Ativação > Update
   routes.post(process.env.APP_BASE_URL+'/ativacao/ativar', 
              (req,res) => routes.controllers.ativacao.Update(routes, req, res)
   );
}