module.exports = (routes) => {
    /**
    * @swagger
    *
    * /users/insert:
    *   post:
    *     security:
    *      - Bearer: []
    *     description: Insere um Usuário
    *     produces:
    *       - application/json
    *     parameters:
    *       - in: formData
    *         name: strname
    *         description: Nome do usuário
    *         required: true
    *         type: string
    *       - in: formData
    *         name: stremail
    *         description: E-mail
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: strphone
    *         description: Telefone
    *         required: false
    *         type: string
    *         format: string
    *       - in: formData
    *         name: struser
    *         description: Usuário
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: strpassword
    *         description: Senha
    *         required: true
    *         type: string
    *         format: password
    *       - in: formData
    *         name: strpermissions
    *         description: Permissões
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: boolactive
    *         description: Ativo / Inativo
    *         required: true
    *         type: boolean
    *         format: boolean
    *       - in: formData
    *         name: boolnotifications
    *         description: Receber notificaões?
    *         required: true
    *         type: boolean
    *         format: boolean
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário cadastrado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > Insert
    routes.post(process.env.APP_BASE_URL+'/users/insert', 
               passport.authenticate('jwt', { session: false }), 
               (req,res) => routes.controllers.users.Create(routes, req, res)
    );
    /**
    * @swagger
    *
    * /users/update/{id}:
    *   put:
    *     security:
    *      - Bearer: []
    *     description: Altera o cadastro do usuário
    *     produces:
    *       - application/json
    *     parameters:
    *       - in: path
    *         name: id
    *         description: Id do usuario
    *         required: true
    *         type: string
    *       - in: formData
    *         name: strname
    *         description: Nome do usuário
    *         required: true
    *         type: string
    *       - in: formData
    *         name: stremail
    *         description: E-mail
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: strphone
    *         description: Telefone
    *         required: false
    *         type: string
    *         format: string
    *       - in: formData
    *         name: struser
    *         description: Usuário
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: strpassword
    *         description: Senha
    *         required: true
    *         type: string
    *         format: password
    *       - in: formData
    *         name: strpermissions
    *         description: Permissões
    *         required: true
    *         type: string
    *         format: string
    *       - in: formData
    *         name: boolactive
    *         description: Ativo / Inativo
    *         required: true
    *         type: boolean
    *         format: boolean
    *       - in: formData
    *         name: boolnotifications
    *         description: Receber notificaões?
    *         required: true
    *         type: boolean
    *         format: boolean
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário alterado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > Update
    routes.put(process.env.APP_BASE_URL+'/users/update/:id', 
               passport.authenticate('jwt', { session: false }), 
               (req,res) => routes.controllers.users.Update(routes, req, res)
    );
    /**
    * @swagger
    *
    * /users/delete/{id}:
    *   delete:
    *     security:
    *      - Bearer: []
    *     description: Apaga o cadastro do usuário
    *     produces:
    *       - application/json
    *     parameters:
    *       - in: path
    *         name: id
    *         description: Id do usuario
    *         required: true
    *         type: string
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário apagado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > Delete
    routes.delete(process.env.APP_BASE_URL+'/users/delete/:id', 
                  passport.authenticate('jwt', { session: false }), 
                  (req,res) => routes.controllers.users.Delete(routes, req, res)
    );
    /**
    * @swagger
    *
    * /users/recover/{id}:
    *   put:
    *     security:
    *      - Bearer: []
    *     description: Recupera o cadastro do usuário
    *     produces:
    *       - application/json
    *     parameters:
    *       - in: path
    *         name: id
    *         description: Id do usuario
    *         required: true
    *         type: string
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário recuperado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > Recover
    routes.put(process.env.APP_BASE_URL+'/users/recover/:id', 
               passport.authenticate('jwt', { session: false }), 
               (req,res) => routes.controllers.users.Recover(routes, req, res)
    );
    /**
    * @swagger
    *
    * /users/list:
    *   get:
    *     security:
    *      - Bearer: []
    *     description: Lista todos os usuários
    *     produces:
    *       - application/json
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário recuperado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > List
    routes.get(process.env.APP_BASE_URL+'/users/list', 
               passport.authenticate('jwt', { session: false }), 
               (req,res) => routes.controllers.users.List(routes, req, res)
    );     
    /**
    * @swagger
    *
    * /users/info/{id}:
    *   get:
    *     security:
    *      - Bearer: []
    *     description: Informações de um usuário em especifico
    *     produces:
    *       - application/json
    *     parameters:
    *       - in: path
    *         name: id
    *         description: Id do usuário
    *         required: true
    *         type: string
    *     tags:
    *      - Users
    *     responses:
    *       200:
    *         description: Usuário recuperado com sucesso!
    *       422:
    *         description: Dado(s) incorreto(s)
    */
    // Route > Users > Info
    routes.get(process.env.APP_BASE_URL+'/users/info/:id', 
               passport.authenticate('jwt', { session: false }), 
               (req,res) => routes.controllers.users.Read(routes, req, res)
    ); 
 }