const Token = require('./tokens.js');
var { Op } = require('sequelize');
function zeroFill(value){
    var number = value
    var width = 11;
    width -= number.toString().length;
    if ( width > 0 ){
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + "";
}
module.exports.EnviarEmail = async (morador) => {
    if(morador){
        var SchemaAtivacoes = require('../schemas/ativacao.js');
        //Gera o Token
        let ativacao = await SchemaAtivacoes.Ativacoes.create({
            intmoradorid: morador.id
        });
        //Se inseriu dispara o e-mail
        if(ativacao){
            var fs = require('fs');
            var template = fs.readFileSync('./app/public/views/template.html', 'utf8');
            const primeiroNome = morador.strnome.split(' ')[0];  
            //Sobrescreve template
            var mensagem = template.replace('{{nomedousuario}}', primeiroNome).replace('{{token}}', ativacao.strcodigo).replace(new RegExp('{{caminho}}', 'g'), process.env.APP_URL_IMGS);
            const Mail = require('../controllers/mail.js');
            return await Mail.EnviarEmail(morador.stremail,'Ativação de conta #'+zeroFill(ativacao.id),mensagem);
        }
      }
}
module.exports.Create = (application, req, res) => {
    const { strnome,
            strcelular,
            strsenha,
            intestadocivilid,
            strcpf,
            strrg,
            stremail } = req.body;
    application.models.moradores.getMoradores({
        strcpf
    }).then(moradores => {
        if(!moradores){
            application.models.moradores.createMoradores({
                strnome,
                strcelular,
                strsenha,
                intestadocivilid,
                strcpf,
                strrg,
                stremail,
                struserinsert: 'sistema'
            }).then(moradoresinserted => {
                res.status(200).json({
                    mensagem: 'Parabéns seu cadastro foi realizado! <br>Enviamos um e-mail com as instruções para ativação da sua conta. <br>Verifique em sua Caixa de entrada, SPAM ou Lixo eletrônico.',
                    moradoresinserted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o morador', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este cpf já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar tente novamente mais tarde', erro: error });
    });
}
module.exports.Update = (application, req, res) => {
    const { id } = req.params,
          { strnome,
            strcelular,
            strsenha,
            intestadocivilid,
            strcpf,
            strrg,
            stremail,
            boolactive } = req.body;
    application.models.moradores.getMoradores({
        strnome,
        strcpf,
        strrg,
        stremail,
        boolactive,
        id: {
            [Op.ne]: id
        }
    }).then(moradores => {
        if(!moradores){
            application.models.moradores.updateMoradores({
                strnome,
                strcelular,
                strsenha,
                intestadocivilid,
                strcpf,
                strrg,
                stremail,
                boolactive,
                struserupdate: Token.getUserIdToken(req)
            }, id).then(moradoresUpdated => {
                res.status(200).json({
                    mensagem: 'Dado(s) alterado(s) com sucesso!',
                    moradoresUpdated
                });
            }).catch((error)=>{
                res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao alterar morador!', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este morador já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar morador', erro: error });
    });
}
module.exports.Read = (application, req, res) => {
    const { id } = req.params;
    application.models.moradores.getMoradores({
        booldeleted: false,
        id
    }).then(moradores => {
        (moradores)? res.status(200).json({ moradores }) : res.status(422).json({ mensagem: 'Morador não encontrado'});
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar morador', erro: error });
    });
}
module.exports.List = (application, req, res) => {
    application.models.moradores.getAllMoradores()
    .then(moradores => {
        res.status(200).json({ moradores });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar os moradores', erro: error });
    });
}
module.exports.Delete = (application, req, res) => {
    const { id } = req.params;
    application.models.moradores.getMoradores({
        booldeleted: false,
        id
    }).then(moradores => {
        if(moradores){
            application.models.moradores.updateMoradores({
                booldeleted: true,
                struserupdate: Token.getUserIdToken(req),
            },id).then(moradores => {
                res.status(200).json({ 
                    mensagem: 'Morador apagado com sucesso!', 
                    moradores
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao apagar morador', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar morador', erro: error });
    });
}
module.exports.Recover = (application, req, res) => {
    const { id } = req.params;
    application.models.moradores.getMoradores({
        booldeleted: true,
        id
    }).then(moradores => {
        if(moradores){
            application.models.moradores.updateMoradores({
                booldeleted: false,
                struserupdate: Token.getUserIdToken(req),
            },id).then(moradoresRecovered => {
                res.status(200).json({ 
                    mensagem: 'Morador recuperado com sucesso!', 
                    moradoresRecovered 
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao recuperar morador', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar morador', erro: error });
    });
}