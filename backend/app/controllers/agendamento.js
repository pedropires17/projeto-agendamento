const Token = require('./tokens.js');
var moment = require('moment');
function zeroFill(value){
    var number = value
    var width = 11;
    width -= number.toString().length;
    if ( width > 0 ){
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + "";
}
function dateBR(value){
    if (!value) return 'Data Inválida';
    return moment(String(value)).format('DD/MM/YYYY')
}
function datetimeBR(value){
    if (!value) return 'Data Inválida';
    return moment(String(value)).format('DD/MM/YYYY HH:mm:ss')
}
module.exports.EnviarEmail = async (agendamento) => {
  //Se possui
  if(agendamento){
    const { QueryTypes } = require('sequelize');
    const db = require('../../config/db');
    await db.query(`SELECT tblagendamentos.id,
                           tblagendamentos.createdAt,
                           tblagendamentos.strturno,
                           tblmoradores.strnome,
                           tblmoradores.stremail,
                           tblmoradores.intestadocivilid,
                           tblcalendarios.dtadisponivel 
                    FROM tblagendamentos
                    INNER JOIN tblmoradores ON
                    (tblmoradores.id = tblagendamentos.intmoradorid) 
                    INNER JOIN tblcalendarios ON
                    (tblcalendarios.id = tblagendamentos.intdtadisponivelid)
                    WHERE tblagendamentos.intmoradorid = ?
                    AND tblagendamentos.id = ?
                    AND tblagendamentos.booldeleted = false
                    ORDER BY tblcalendarios.dtadisponivel DESC`, { replacements: [agendamento.intmoradorid,agendamento.id],type: QueryTypes.SELECT})
    .then(async(resultado)=>{
        if(resultado && resultado.length > 0){
            let modelos = ['<strong>BENEFICIÁRIO SOLTEIRO (A):</strong><br>Comparecer levando os seguintes documentos:<br>- RG/CPF<br>- TÍTULO DE ELEITOR<br>- CERTIDÃO DE NASCIMENTO<br>- COMPROVANTE DE RESIDÊNCIA<br><br>',
                '<strong>BENEFICIÁRIO CASADO (A):</strong><br>Comparecer levando os seguintes documentos:<br>- RG/CPF<br>- TÍTULO DE ELEITOR<br>- CERTIDÃO DE NASCIMENTO<br>- COMPROVANTE DE RESIDÊNCIA<br><br>',
                '<strong>BENEFICIÁRIO VIÚVO (A):</strong><br>Comparecer levando os seguintes documentos:<br>- RG/CPF<br>- TÍTULO DE ELEITOR<br>- CERTIDÃO DE ÓBITO<br>- COMPROVANTE DE RESIDÊNCIA<br><br>',
                '<strong>BENEFICIÁRIO DIVORCIADO (A):</strong><br>Comparecer levando os seguintes documentos:<br>- RG/CPF<br>- TÍTULO DE ELEITOR<br>- AVERBAÇÃO<br>- COMPROVANTE DE RESIDÊNCIA<br><br>'];
            var fs = require('fs');
            var template = fs.readFileSync('./app/public/views/template_comprovante.html', 'utf8').toString();
            //Sobrescreve template
            let turno = '';
            if(resultado[0].strturno === 'M') turno = ' - Matutino';
            if(resultado[0].strturno === 'V') turno = ' - Vespertino';
            var mensagem = template.replace('{{numero}}', zeroFill(resultado[0].id))
                                   .replace('{{nome}}', resultado[0].strnome)
                                   .replace('{{dtasolicitacao}}', datetimeBR(resultado[0].createdAt))
                                   .replace('{{dtaagendada}}', dateBR(resultado[0].dtadisponivel)+turno)
                                   .replace('{{instrucoes}}', modelos[parseInt(resultado[0].intestadocivilid) - 1])
                                   .replace(new RegExp('{{caminho}}', 'g'), process.env.APP_URL_IMGS);
            const Mail = require('../controllers/mail.js');
            return await Mail.EnviarEmail(resultado[0].stremail,'Comprovante de Agendamento #'+zeroFill(resultado[0].id),mensagem);
        }
    });
  }
}
module.exports.Create = (application, req, res) => {
    const { intdtadisponivelid,
            strturno,
            intmoradorid } = req.body;
    application.models.agendamento.getAgendamento({
        intdtadisponivelid,
        strturno,
        intmoradorid
    }).then(agendamento => {
        if(!agendamento){
            application.models.agendamento.createAgendamento({
                intdtadisponivelid,
                strturno,
                intmoradorid,
                strip: JSON.stringify(req.ipInfo),
                boolactive: true,
            }).then(agendamentoinserted => {
                res.status(200).json({
                    mensagem: 'Agendamento cadastrado com sucesso!',
                    agendamentoinserted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o agendamento', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este agendamento já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar agendamento', erro: error });
    });
}
module.exports.Update = (application, req, res) => {
    const { id } = req.params,
          { intdtadisponivelid,
            intmoradorid,
            boolactive } = req.body;
    application.models.agendamento.getAgendamento({
        intdtadisponivelid,
        intmoradorid,
        boolactive,
        id: {
            [Op.ne]: id
        }
    }).then(agendamento => {
        if(!agendamento){
            application.models.agendamento.updateAgendamento({
                intdtadisponivelid,
                intmoradorid,
                strip: req.ipInfo,
                boolactive
            }, id).then(agendamentoUpdated => {
                res.status(200).json({
                    mensagem: 'Dado(s) alterado(s) com sucesso!',
                    agendamentoUpdated
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao alterar morador!', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este agendamento já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar agendamento', erro: error });
    });
}
module.exports.Read = (application, req, res) => {
    const { id } = req.params;
    application.models.agendamento.getAgendamento({
        booldeleted: false,
        id
    }).then(agendamento => {
        (agendamento)? res.status(200).json({ agendamento }) : res.status(422).json({ mensagem: 'Agendamento não encontrado'});
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar agendamento', erro: error });
    });
}
module.exports.List = (application, req, res) => {
    application.models.agendamento.getAllAgendamento()
    .then(agendamento => {
        res.status(200).json({ agendamento });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar os agendamentos', erro: error });
    });
}
module.exports.ListMeusAgendamentos = (application, req, res) => {
    application.models.agendamento.getAllMeusAgendamentos(Token.getUserIdToken(req))
    .then(agendamento => {
        res.status(200).json({ agendamento });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar os agendamentos', erro: error });
    });
}
module.exports.Delete = (application, req, res) => {
    const { id } = req.params;
    application.models.agendamento.getAgendamento({
        booldeleted: false,
        id
    }).then(agendamento => {
        if(agendamento){
            application.models.agendamento.updateAgendamento({
                booldeleted: true,
                struserupdate: Token.getUserIdToken(req),
            },id).then(agendamento => {
                res.status(200).json({ 
                    mensagem: 'Agendamento apagado com sucesso!', 
                    agendamento
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao apagar agendamento', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar agendamento', erro: error });
    });
}
module.exports.Recover = (application, req, res) => {
    const { id } = req.params;
    application.models.agendamento.getAgendamento({
        booldeleted: true,
        id
    }).then(agendamento => {
        if(agendamento){
            application.models.agendamento.updateAgendamento({
                booldeleted: false,
                struserupdate: Token.getUserIdToken(req),
            },id).then(agendamentoRecovered => {
                res.status(200).json({ 
                    mensagem: 'Agendamento recuperado com sucesso!', 
                    agendamentoRecovered 
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao recuperar agendamento', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar agendamento', erro: error });
    });
}