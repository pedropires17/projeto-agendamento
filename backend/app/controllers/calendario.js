const Token = require('./tokens.js');
var { Op } = require('sequelize');
module.exports.Create = (application, req, res) => {
    const { dtadisponivel,
            intqtd,
            boolactive } = req.body;
    application.models.calendario.getCalendario({
        dtadisponivel,
        boolactive
    }).then(calendario => {
        if(!calendario){
            application.models.calendario.createCalendario({
                dtadisponivel,
                intqtd,
                boolactive,
                struserinsert: Token.getUserIdToken(req)
            }).then(calendarioinserted => {
                res.status(200).json({
                    mensagem: 'Calendário cadastrado com sucesso!',
                    calendarioinserted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o calendário', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este calendário já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar calendário', erro: error });
    });
}
module.exports.Update = (application, req, res) => {
    const { id } = req.params,
          { dtadisponivel,
            intqtd,
            boolactive } = req.body;
    application.models.calendario.getCalendario({
        dtadisponivel,
        id: {
            [Op.ne]: id
        }
    }).then(calendario => {
        if(!calendario){
            application.models.calendario.updateCalendario({
                dtadisponivel,
                intqtd,
                boolactive,
                struserupdate: Token.getUserIdToken(req)
            }, id).then(calendarioUpdated => {
                res.status(200).json({
                    mensagem: 'Dado(s) alterado(s) com sucesso!',
                    calendarioUpdated
                });
            }).catch((error)=>{
                res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao alterar morador!', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este Calendário já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar calendário', erro: error });
    });
}
module.exports.Read = (application, req, res) => {
    const { id } = req.params;
    application.models.calendario.getCalendario({
        booldeleted: false,
        id
    }).then(calendario => {
        (calendario)? res.status(200).json({ calendario }) : res.status(422).json({ mensagem: 'Calendário não encontrado'});
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar calendário', erro: error });
    });
}
module.exports.List = (application, req, res) => {
    application.models.calendario.getAllCalendario()
    .then(calendario => {
        res.status(200).json({ calendario });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar calendário', erro: error });
    });
}
module.exports.ListDisponivel = (application, req, res) => {
    application.models.calendario.getAllDisponiveis()
    .then(calendario => {
        res.status(200).json({ calendario });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar calendário', erro: error });
    });
}
module.exports.Delete = (application, req, res) => {
    const { id } = req.params;
    application.models.calendario.getCalendario({
        booldeleted: false,
        id
    }).then(calendario => {
        if(calendario){
            application.models.calendario.updateCalendario({
                booldeleted: true,
                struserupdate: Token.getUserIdToken(req),
            },id).then(calendario => {
                res.status(200).json({ 
                    mensagem: 'Calendário apagado com sucesso!', 
                    calendario
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao apagar calendário', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar calendário', erro: error });
    });
}
module.exports.Recover = (application, req, res) => {
    const { id } = req.params;
    application.models.calendario.getCalendario({
        booldeleted: true,
        id
    }).then(calendario => {
        if(calendario){
            application.models.calendario.updateCalendario({
                booldeleted: false,
                struserupdate: Token.getUserIdToken(req),
            },id).then(calendarioRecovered => {
                res.status(200).json({ 
                    mensagem: 'Calendário recuperado com sucesso!', 
                    calendarioRecovered 
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao recuperar calendário', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Registro não encontrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar calendário', erro: error });
    });
}