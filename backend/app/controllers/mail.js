function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
module.exports.EnviarEmail = async (destino, assunto, mensagem) => {
    try{
        const nodemailer = require('nodemailer');
        let sorteio = getRandomInt(1,2);
        let smtp = {
            host: (sorteio === 1)? process.env.MAIL_HOST1 : process.env.MAIL_HOST2,
            port: (sorteio === 1)? process.env.MAIL_PORT1 : process.env.MAIL_PORT2,
            secureConnection: false,
            auth: {
                user: (sorteio === 1)? process.env.MAIL_USERNAME1 : process.env.MAIL_USERNAME2,
                pass: (sorteio === 1)? process.env.MAIL_PASSWORD1: process.env.MAIL_PASSWORD2
            },
            tls:{
                ciphers:'SSLv3'
            }
        };
        const transporter = nodemailer.createTransport(smtp);
        let remetente = (sorteio === 1)? process.env.MAIL_USERNAME1 : process.env.MAIL_USERNAME2;
        const mailOptions = {
            from: 'Agendamento Online <'+remetente+'>',
            to: destino,
            subject: assunto,
            html: mensagem
        };
        await transporter.sendMail(mailOptions, async(error, info) => {
            const Emails = require('../models/emails.js');
            await Emails.createEmail({
                strremetente: remetente,
                strdestino: destino,
                strassunto: assunto,
                stmensagem: mensagem,
                smtpresponse: JSON.stringify(info),
                boolenviado: (error)? false : true
            });
            if (error) transporter.close(), console.log(error);
            console.log('Email enviado: ' + info.response);
            transporter.close();
        });
    }catch (e) {
        console.warn(e);
    }
}