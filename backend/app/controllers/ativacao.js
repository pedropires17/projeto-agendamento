const Token = require('./tokens.js');
var { Op } = require('sequelize');
function zeroFill(value){
    var number = value
    var width = 11;
    width -= number.toString().length;
    if ( width > 0 ){
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + "";
}
module.exports.EnviarEmail = async (ativacao) => {
    //Se for recuperação de Senha
    if(ativacao && ativacao.inttipoid === 1){
        var SchemaMoradores = require('../schemas/moradores.js');
        let morador = await SchemaMoradores.Moradores.findOne({ where: { id: ativacao.intmoradorid } });
        var fs = require('fs');
        var template = fs.readFileSync('./app/public/views/template_recuperacao.html', 'utf8');
        const primeiroNome = morador.strnome.split(' ')[0];  
        //Sobrescreve template
        var mensagem = template.replace('{{nomedousuario}}', primeiroNome).replace('{{token}}', ativacao.strcodigo).replace('{{email}}', morador.stremail).replace(new RegExp('{{caminho}}', 'g'), process.env.APP_URL_IMGS);
        const Mail = require('../controllers/mail.js');
        return await Mail.EnviarEmail(morador.stremail,'Recuperação de senha #'+zeroFill(ativacao.id),mensagem);
    }
}
module.exports.Create = (application, req, res) => {
    const { intmoradorid ,
            inttipoid } = req.body;
    application.models.ativacao.getAtivacao({
        intmoradorid ,
        inttipoid
    }).then(ativacao => {
        if(!ativacao){
            application.models.ativacao.createAtivacao({
                intmoradorid ,
                inttipoid,
                struserinsert: Token.getUserIdToken(req)
            }).then(ativacaoinserted => {
                res.status(200).json({
                    mensagem: 'Ativação / Recuperação cadastrada com sucesso!',
                    ativacaoinserted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o ativação / recuperação', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Esta ativação / recuperação já foi cadastrada!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar ativação / recuperação', erro: error });
    });
}
module.exports.Recuperar = (application, req, res) => {
    const { stremail,
            strcpf } = req.body;
    application.models.moradores.getMoradores({
        stremail,
        strcpf
    }).then(morador => {
        const { Op } = require("sequelize");
        var moment = require('moment');
        application.models.ativacao.getAtivacao({
            intmoradorid: morador.id,
            inttipoid: 1,
            boolused: false,
            createdAt: {
                [Op.startsWith]: moment().format('YYYY-MM-DD'),
            }
        }).then(recuperacao => {
            if(!recuperacao){
                if(morador){
                    application.models.ativacao.createAtivacao({
                        intmoradorid: morador.id,
                        inttipoid: 1,
                        struserinsert: 'sistema'
                    }).then(ativacaoinserted => {
                        res.status(200).json({
                            mensagem: 'Recuperação de senha solicitada com sucesso! Enviamos um e-mail com as instruções para recuperação da senha. Verifique em sua Caixa de entrada, SPAM ou Lixo eletrônico.',
                            ativacaoinserted
                        });
                    }).catch((error)=>{
                        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o ativação / recuperação', erro: error });
                    });
                } else {
                    res.status(422).json({
                        mensagem: 'Dado(s) inválidos'
                    });
                }
            } else {
                res.status(422).json({
                    mensagem: 'Você já solicitou a recuperação de senha.'
                });
            }
        }).catch((error)=>{
            res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Dado(s) incorreto(s)', erro: error });
        });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Dado(s) incorreto(s)', erro: error });
    });
}
module.exports.AlterarDados = (application, req, res) => {
    const { stremail,
            strtoken,
            strsenha } = req.body;
    application.models.moradores.getMoradores({
        stremail
    }).then(morador => {
        application.models.ativacao.getAtivacao({
            intmoradorid: morador.id,
            inttipoid: 1,
            strcodigo: strtoken,
            boolused: false,
            boolexpire: false,
        }).then(recuperacao => {
            if(recuperacao){
                if(morador){
                    //Baixa o token
                    application.models.ativacao.updateAtivacao({
                        boolused: true
                    }, recuperacao.id)
                    .then((token)=>{
                        //Altera a Senha
                        application.models.moradores.updateMoradores({
                            strsenha
                        }, morador.id).then(moradoresUpdated => {
                            res.status(200).json({
                                mensagem: 'Dado(s) alterado(s) com sucesso!',
                                moradoresUpdated
                            });
                        }).catch((error)=>{
                            res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao alterar dado(s)!', erro: error });
                        });
                    }).catch((error)=>{
                        res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao baixar o token!', erro: error });
                    });
                } else {
                    res.status(422).json({
                        mensagem: 'Dado(s) inválidos'
                    });
                }
            } else {
                res.status(422).json({
                    mensagem: 'Dados(s) inválido(s).'
                });
            }
        }).catch((error)=>{
            res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Dado(s) incorreto(s)', erro: error });
        });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Dado(s) incorreto(s)', erro: error });
    });
}
module.exports.Update = (application, req, res) => {
    const { strtoken } = req.body;
    application.models.ativacao.getAtivacao({
        strcodigo: strtoken,
        inttipoid: 2
    }).then(ativacao => {
        if(ativacao){
            if(ativacao.boolused){
                res.status(422).json({
                    mensagem: 'O Token informado já foi utilizado!'
                });
            } else {
                //Baixa o token
                application.models.ativacao.updateAtivacao({
                    boolused: true
                }, ativacao.id).then(ativacaoUpdated => {
                    //Ativa o usuário
                    application.models.moradores.updateMoradoresAtivacao({
                        boolactive: true
                    },{ id : ativacao.intmoradorid }).then((retorno)=>{
                        res.status(200).json({
                            mensagem: 'Parabéns! Sua conta foi ativada com sucesso e você já pode utilizar o sistema, basta realizar seu login e solicitar o seu agendamento.',
                        });
                    }).catch((error)=>{
                        res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao ativar o usuário!', erro: error });
                    });
                }).catch((error)=>{
                    res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao baixar o token!', erro: error });
                });
            }
        } else {
            res.status(422).json({
                mensagem: 'O Token informado é inválido!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar ativação / recuperação', erro: error });
    });
}