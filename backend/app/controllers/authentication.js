module.exports.Login = (application, req, res) => {
  const { struser,
          strpassword } = req.body;
  let bcrypt = require('bcrypt');
  if (struser && strpassword) {
    application.models.users.getAuthUser({
        struser: struser,
        boolactive: true,
        booldeleted: false
    }).then(user => {
      if (!user) {
          res.status(422).json({ mensagem: 'Dado(s) incorreto(s)' });
      } else if (bcrypt.compareSync(strpassword, user.strpassword)) {
        let userData = { 
          id: user.id,
          strname: user.strname.split(' ',)[0],
          struser: user.struser,
          strpermissions: user.strpermissions,
        };
        var token = jwt.sign(userData, opcoesJWT.secretOrKey, { expiresIn: process.env.APP_TIME_JWT, algorithm: 'RS256' });
        res.status(200).json({ 
          mensagem: 'Logado com sucesso!',
          token: token,
          user: userData
        });
      } else {
        res.status(422).json({
          mensagem: 'Dado(s) incorreto(s)'
        });
      }
    });
  } else {
    res.status(422).json({
      mensagem: 'Parâmetro(s) incorreto(s)'
    });
  }
}
module.exports.LoginMorador = (application, req, res) => {
  const { stremail,
          strsenha } = req.body;
  let bcrypt = require('bcrypt');
  if (stremail && strsenha) {
    application.models.moradores.getMoradores({
        stremail: stremail,
        booldeleted: false
    }).then(morador => {
      if (!morador) {
          res.status(422).json({ mensagem: 'Dado(s) incorreto(s)' });
      } else if (bcrypt.compareSync(strsenha, morador.strsenha)) {
        if(morador.boolactive === true){
          let moradorData = { 
            id: morador.id,
            intestadoid: morador.intestadocivilid,
            strnome: morador.strnome.split(' ',)[0],
            strpermissions: 'morador',
          };
          var token = jwt.sign(moradorData, opcoesJWT.secretOrKey, { expiresIn: process.env.APP_TIME_JWT, algorithm: 'RS256' });
          res.status(200).json({ 
            mensagem: 'Logado com sucesso!',
            token: token,
            user: moradorData
          });
        } else {
          res.status(422).json({
            mensagem: 'Você precisa ativar sua conta para acessar o sistema. Verifique seu e-mail e siga as instruções para ativação da sua conta.'
          });
        }
      } else {
        res.status(422).json({
          mensagem: 'Dado(s) incorreto(s)'
        });
      }
    });
  } else {
    res.status(422).json({
      mensagem: 'Dado(s) incompleto(s)'
    });
  }
}