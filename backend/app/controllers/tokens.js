//Usuário > Logado
module.exports = { 
  RefreshToken(cabecalho) {
    const usertoken = cabecalho.headers.authorization;
    const tokenjwt = usertoken.split(' ');
    const jwt_payload = jwt.verify(tokenjwt[1], process.env.APP_Chave, { algorithms: ['RS256'] });
    var payload = {
      strusuario: (jwt_payload.strusuario) ? jwt_payload.strusuario : '',
      nome: (jwt_payload.nome) ? jwt_payload.nome : '',
      intperfilid: (jwt_payload.intperfilid) ? jwt_payload.intperfilid : '',
    };
    var token = jwt.sign(payload, process.env.APP_Chave_Privada, {
      expiresIn: process.env.APP_Tempo_JWT,
      algorithm: 'RS256'
    });
    return token
  },
  getUserIdToken(cabecalho) {
    const usertoken = cabecalho.headers.authorization;
    const tokenjwt = usertoken.split(' ');
    const jwt_payload = jwt.verify(tokenjwt[1], process.env.APP_Chave, { algorithms: ['RS256'] });
    return jwt_payload.id;
  },
  getNameToken(cabecalho) {
    const usertoken = cabecalho.headers.authorization;
    const tokenjwt = usertoken.split(' ');
    const jwt_payload = jwt.verify(tokenjwt[1], process.env.APP_Chave, { algorithms: ['RS256'] });
    return jwt_payload.strnome;
  },
  getPermissions(cabecalho) {
    const usertoken = cabecalho.headers.authorization;
    const tokenjwt = usertoken.split(' ');
    const jwt_payload = jwt.verify(tokenjwt[1], process.env.APP_Chave, { algorithms: ['RS256'] });
    return jwt_payload.strpermissions;
  },
  ChecaPermissao(array, req, res){
    //Habilita contains
    Array.prototype.contains = function ( localizar ) {
      for (item in this) {
        if (this[item] == localizar) return true;
      }
      return false;
    }
    const Token = require('../controllers/tokens.js');
    let permissao = Token.getPerfilToken(req);
    if(!array.contains(permissao)){
      res.status(403).json({ mensagem: 'Você não possui permissão para usar este módulo' });
      throw new Error('Você não possui permissão para usar este módulo');
    }
    //Renova o token do usuário
    res.header('Authorization', 'Bearer '+ Token.RenovarToken(req));
    res.header("Access-Control-Expose-Headers", "Authorization");
  }
 }