const Token = require('../controllers/tokens.js');
var { Op } = require('sequelize');
module.exports.Create = (application, req, res) => {
    const { strname,
            strphone,
            stremail,
            struser,
            strpassword,
            strpermissions,
            boolactive,
            boolnotifications} = req.body;
    application.models.users.getUser({
        stremail: stremail,
        struser: {
            [Op.iLike]: struser
        },
        boolactive
    }).then(users => {
        if(!users){
            application.models.users.createUser({
                strname,
                strphone,
                stremail,
                struser,
                strpassword,
                strpermissions,
                boolactive,
                boolnotifications,
                struserinsert: Token.getUserIdToken(req)
            }).then(usersinserted => {
                res.status(200).json({
                    mensagem: 'Usuário cadastrado com sucesso!',
                    usersinserted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao salvar o usuário', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este usuário já foi cadastrado!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar usuário', erro: error });
    });
}
module.exports.Update = (application, req, res) => {
    const { id } = req.params,
          { strname,
            strphone,
            stremail,
            struser,
            strpassword,
            strpermissions,
            boolactive,
            boolnotifications, } = req.body;
    application.models.users.getUser({
        struser: {
            [Op.iLike]: struser
        },
        stremail: stremail,
        boolactive,
        id: {
            [Op.ne]: id
        }
    }).then(users => {
        if(!users){
            application.models.users.updateUser({
                strname,
                strphone,
                stremail,
                struser,
                strpassword,
                strpermissions,
                boolactive,
                boolnotifications,
                struserupdate: Token.getUserIdToken(req)
            }, id).then(usersUpdated => {
                res.status(200).json({
                    mensagem: 'Usuário alterado com sucesso!',
                    usersUpdated
                });
            }).catch((error)=>{
                res.status(422).json({  mensagem: (error.errors)? error.errors[0].message : 'Erro ao alterar usuário!', erro: error });
            });
        } else {
            res.status(422).json({
                mensagem: 'Este usuário já foi cadastrada!'
            });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar usuário', erro: error });
    });
}
module.exports.Read = (application, req, res) => {
    const { id } = req.params;
    application.models.users.getUser({
        booldeleted: false,
        id
    }).then(users => {
        (users)? res.status(200).json({ users }) : res.status(422).json({ mensagem: 'Usuário não encontrada'});
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar usuário', erro: error });
    });
}
module.exports.List = (application, req, res) => {
    application.models.users.getAllUsers()
    .then(users => {
        res.status(200).json({ users });
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao listar os usuários', erro: error });
    });
}
module.exports.Delete = (application, req, res) => {
    const { id } = req.params;
    application.models.users.getUser({
        booldeleted: false,
        id
    }).then(users => {
        if(users){
            application.models.users.updateUser({
                booldeleted: true,
                struserupdate: Token.getUserIdToken(req),
            },id).then(usersDeleted => {
                res.status(200).json({ 
                    mensagem: 'Usuário apagado com sucesso!', 
                    usersDeleted
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao apagar usuário', erro: error });
            });
        } else {
            res.status(422).json({ mensagem: 'Registro não encontrado!' });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar usuário', erro: error });
    });
}
module.exports.Recover = (application, req, res) => {
    const { id } = req.params;
    application.models.users.getUser({
        booldeleted: true,
        id
    }).then(users => {
        if(users){
            application.models.users.updateUser({
                booldeleted: false,
                struserupdate: Token.getUserIdToken(req),
            },id).then(usersRecovered => {
                res.status(200).json({ 
                    mensagem: 'Usuário recuperado com sucesso!', 
                    usersRecovered
                });
            }).catch((error)=>{
                res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao recuperar usuário', erro: error });
            });
        } else {
            res.status(422).json({ mensagem: 'Registro não encontrado!' });
        }
    }).catch((error)=>{
        res.status(422).json({ mensagem: (error.errors)? error.errors[0].message : 'Erro ao consultar usuário', erro: error });
    });
}