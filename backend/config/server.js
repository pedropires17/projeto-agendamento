const express = require('express'),
      bodyParser = require('body-parser'),
      load = require('consign'),
      morgan = require('morgan'),
      cors = require('cors');
      global.passport = require('passport'),
      opcoesJWT = {},
      jwt = require('jsonwebtoken'),
      passportJWT = require('passport-jwt'),
      ExtractJwt = passportJWT.ExtractJwt,
      helmet = require('helmet'),
      JwtStrategy = passportJWT.Strategy,
      expressip = require('express-ip');
opcoesJWT.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opcoesJWT.secretOrKey = process.env.APP_Chave_Privada;
opcoesJWT.algorithms =  ["RS256"];
//Configuração JSON Webtoken
var regraJWT = new JwtStrategy(opcoesJWT, function (jwt_payload, next) {
  if(process.env.APP_SERVER_DEBUG == 'S') console.log('payload received', jwt_payload);
  if(jwt_payload.strpermissions === 'morador'){
    app.models.moradores.getMoradorJWT({
      id: jwt_payload.id,
      boolactive: true,
      booldeleted: false,
    }).then(userdb => {
        if(process.env.APP_SERVER_DEBUG == 'S') console.log(userdb);
        (userdb) ? next(null, userdb): next(null, false);
    });
  } else {
    app.models.users.getUserJWT({
      id: jwt_payload.id,
      struser: jwt_payload.struser,
      boolactive: true,
      booldeleted: false,
    }).then(userdb => {
        if(process.env.APP_SERVER_DEBUG == 'S') console.log(userdb);
        (userdb) ? next(null, userdb): next(null, false);
    });
  }
});
const app = express();
//Habilita ips
app.set('trust proxy', true);
app.use(expressip().getIpInfoMiddleware);
//Autenticação JWT
passport.use(regraJWT);
if(process.env.APP_SERVER_DEBUG == 'S') app.use(morgan('dev'));
app.use(helmet());
app.disable('x-powered-by');
app.use(process.env.APP_BASE_URL+'/img', express.static(process.env.APP_DIR_IMGS));
(process.env.APP_USAR_CORS != 'S')? app.use(cors()) : app.use(cors({ origin: [process.env.APP_DOMAIN_CORS] }));
app.use(passport.initialize());
app.use(bodyParser.json());
//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));
const swaggerJsdoc = require('swagger-jsdoc'),
      options = {
        swaggerDefinition: {
          swagger: '2.0',
          info: {
            title: 'Sistema de Agendamentos',
            version: '1.0.0',
            description: 'Documentação da API do Sistema de Agendamentos',
            contact: {
              name: '@andbrslz',
              email: 'andbrslz@outlook.com'
            }
          },
          basePath: process.env.APP_BASE_URL,
          securityDefinitions: {
            Bearer: {
              type: 'apiKey',
              template: 'Bearer {apiKey}',
              name: 'Authorization',
              in: 'header',
            }
          },
          schemes: process.env.APP_SWAGGER_PROTOCOLOS
        },
        apis: ['./app/routes/*.js'],
      },
      specs = swaggerJsdoc(options),
      swaggerUi = require('swagger-ui-express');
app.use(process.env.APP_BASE_URL+'/docs', swaggerUi.serve, swaggerUi.setup(specs));
  load({
    cwd: 'app'
  })
  .include('../config/db.js')
  .then('schemas')
  .then('models')
  .then('routes')
  .then('controllers')
  .into(app);
module.exports = app;