// Dados BD
const Sequelize = require('sequelize');
const sequelize = new Sequelize({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  dialect: process.env.DB_TYPE,
  logging: (process.env.APP_ORM_DEBUG == 'S')? true: false,
  dialectOptions: {
    dateStrings: true,
    typeCast: true,
    useUTC: false,
  },
  define: {
    hooks: {
      afterCreate() {}
    }
  },
  timezone: '-03:00',
});
// Conecta ao BD
sequelize
  .authenticate()
  .then(() => {
    if(process.env.APP_ORM_DEBUG == 'S') console.log('Conectado ao banco de dados!');
  })
  .catch(err => {
    if(process.env.APP_ORM_DEBUG == 'S') console.log('Não foi possível conectar a base de dados ', err);
  });
module.exports = sequelize;