require('dotenv').config();
const app = require('./config/server'),
      port = Number(process.env.PORT) || Number(process.env.APP_PORT);
app.listen(port, () => console.log(`Server Online (` + port + `)`));